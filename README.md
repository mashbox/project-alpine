# README #

This is the Bazaarvoice Project Alpine landing page.

### Installation ###


* [Node.js](https://nodejs.org/)
* [Gulp](http://gulpjs.com/)

Once you have the dependencies installed, clone this repository to your local computer and run ```npm install && bower install```. This will install all additional dependencies for Node and Gulp.

#### Gulp Tasks ####
Within Gulp you can run several tasks to make your life easier in development, from within your install directory just enter on of the following:

```
gulp
```

This is the default task that will build your raw files into the correct structure and watch the files for changes. If a change is detected, it will refresh the browser window.

```
gulp build
```
This tasks will take the original source files, run any tasks associated—like CSS precompiling, and output a ZIP file ready for upload into the dist directory in the root.