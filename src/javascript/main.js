$(document).ready(function () {

  var App = {};

  var getParameter = function (name, search) {
    search = search || location.search;
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  var capitalizeFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };


  var updateSlugs = (function(){
    var retailer = getParameter('retailer');
    if (retailer) {
     // Replace [[retailer]] text
     $('span[data-retailer]').text(function () {
       return $(this).text().replace('[[retailer]]', capitalizeFirstLetter(retailer));
     });
    } else {
      $('span[data-retailer]').text(function () {
        return $(this).text().replace('[[retailer]]', 'Your Brand');
      });
    }

    // retailer code storage and setup
    if ($('a[data-code]').length) {
      var code = $('a[data-code]').data('code');
      if (code.length > 0) {
        sessionStorage.setItem('retailerCode',code);
      }
    }
    var retailerCode = sessionStorage.getItem('retailerCode');
    if (retailerCode != null) {
      $('a[data-code]').attr('href',function(x, href){
        return href + '?retailer=' + retailerCode
      });
    }

  })();

  $(window).bind('load scroll', function(){
    if ($(window).scrollTop() >= 30) {
      $('.navbar').addClass('small');
    } else {
      $('.navbar').removeClass('small');
    }
  });

  $('#featurette_button').click(function(e){
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $("#pricing").offset().top - 65
    }, 500);
  });
  /*
  *
  */
  var updateDonut = function ($el) {
    var radius = $el.width() * 0.34;
    var percent = $el.attr('data-percent');
    var circumference = 2 * radius * Math.PI;
    var offset = circumference - (circumference * percent);
    var bkg = $el.find('.bkg');
    var chart = $el.find('.cht');

    bkg.attr('r', radius);
    chart.attr('r', radius);
    chart.attr('stroke-dasharray', circumference);
    chart.attr('stroke-dashoffset', offset);
  };

  var onResize = function () {
    var $donuts = $('.chart');
    _.each($donuts, function(donut) {
      updateDonut($(donut));
    });

    $('input[type="range"]').rangeslider('update', true);
  };


  $(window).resize(_.debounce(onResize, 300));

  var $stars = $('.rateYo');
  $stars.rateYo({
    spacing: "8px",
    ratedFill: '#e3e961',
    starSvg: '<svg xmlns="http://www.w3.org/2000/svg" width="39" height="38" viewBox="0 0 39 38"><path  d="M37.946,14.833l-8.5,9.258L31.43,36.466,19.937,31.294,8.7,37l1.4-12.453L1.169,15.692l12.359-2.525L19.243,2l6.237,10.894Z"/></svg>',
    readOnly: true
  });

  var $divisor = $('.divisor');
  var $figure = $('figure');
  var $count = $('.review_count');
  $('input[type="range"]').rangeslider({
    // Feature detection the default is `true`.
    // Set this to `false` if you want to use
    // the polyfill also in Browsers which support
    // the native <input type="range"> element.
    polyfill: false,

    // Default CSS classes
    rangeClass: 'rangeslider',
    fillClass: 'rangeslider__fill',
    handleClass: 'rangeslider__handle',

    // Callback function
    onInit: function(value) {
      $stars.rateYo("option", "rating", 4.5);
      $('.dot.three .tooltip').addClass('active');
    },

    // Callback function
    onSlide: function(position, value) {
      $divisor.css('width', value + '%');
      $count.html(value);

      // Change star ratings
      if (value < 1) {
        $stars.rateYo("option", "rating", 0);
      }
      if (value > 0 && value < 10) {
        $stars.rateYo("option", "rating", 4);
      }
      if (value > 10) {
        $stars.rateYo("option", "rating", 4.5);
      }

      // Trigger tooltips
      var $dotOne = $('.dot.one .tooltip');
      var $dotTwo = $('.dot.two .tooltip');
      var $dotThree = $('.dot.three .tooltip');
      var $dotFour = $('.dot.four .tooltip');

      if (value == 1) {
        $dotOne.addClass('active');
        $divisor.css('width', '1.5%');
      } else {
        $dotOne.removeClass('active');
      }
      if (value == 10) {
        $dotTwo.addClass('active');
        $divisor.css('width', '9.5%');
      } else {
        $dotTwo.removeClass('active');
      }
      if (value == 50) {
        $dotThree.addClass('active');
      } else {
        $dotThree.removeClass('active');
      }
      if (value == 100) {
        $dotFour.addClass('active');
      } else {
        $dotFour.removeClass('active');
      }

    },

    // Callback function
    onSlideEnd: function(position, value) {}
  });

  /*
  * Carousel click function
  */
  $('.carousel li.brand').click(function(){
    var $this = $(this);
    $this.addClass('selected').siblings().removeClass('selected');
    setTimeout(function () {
      $this.prependTo($('.carousel ul'));
    }, 500);
  });



  // function carouselTimeout() {
  //   $('.carousel li.brand:first-of-type').appendTo($('.carousel ul'));
  //   setTimeout(carouselTimeout, 5000);
  // }
  // carouselTimeout();


  /*
    * Modal Form submit
    */
    $('.share_modal_btn').click(function(){
      var $to = $.trim($('#send_to').val());
      var $from = $.trim($('#from').val());
      var $subject = $.trim($('#subject').val());
      var $message = $.trim($('#message_area').val());

      if (_.isEmpty($to)) {
        notie.alert(2, 'Please complete the To field.');
        return;
      }
      if (_.isEmpty($from)){
        notie.alert(2, 'Please complete the From field.');
        return;
      }
      if (_.isEmpty($subject)){
        notie.alert(2, 'Please enter a subject.');
        return;
      }
      if (_.isEmpty($message)){
        notie.alert(2, 'Please enter a message.');
        return;
      }
      $.ajax({
        url: 'http://api.mashbox.com/bazaarvoice/sendgrid.json',
        method: 'POST',
        data: {
          to: $to,
          from: $from,
          subject: $subject,
          html: $message
        }
      })
      .done(function(data){
        notie.alert(1, 'Your email has been sent!', 2);
        $('#email_modal').modal('hide');
      })
      .fail(function(err){
        notie.alert(2, 'Something went wrong, please try again', 3);
      });
    });


    /*
    * CREATE ACCOUNT SUBMIT
    */
      // $('#signup-form button').click(function(e){
      //   e.preventDefault();
      //   var $first = $.trim($('#first_name').val());
      //   var $last = $.trim($('#last_name').val());
      //   var $email = $.trim($('#email').val());
      //   var $subscription = $('#subscription').val();
      //
      //   if (_.isEmpty($first)) {
      //     notie.alert(2, 'Please enter your first name.');
      //     return;
      //   }
      //
      //   if (_.isEmpty($last)) {
      //     notie.alert(2, 'Please enter your last name.');
      //     return;
      //   }
      //
      //   if (_.isEmpty($email)) {
      //     notie.alert(2, 'Please enter your email address.');
      //     return;
      //   }
      //
      //   if (_.isEmpty($subscription)) {
      //     notie.alert(2, 'Please select a subscription.');
      //     return;
      //   }
      //
      //   var url = `https://bazaarvoice.recurly.com/subscribe/${$subscription}?first_name=${$first}&last_name=${$last}&email=${$email}`;
      //
      //   $('#signup-form').hide();
      //   $('#recurly-form').attr('src', url);
      //   $('.iframe').show();
      //
      // });

      $('#show-password').on('change', function () {
        var $this = $(this);
        var type = $this.is(':checked') ? 'text' : 'password';
        $('#password').attr('type', type);
      });

  onResize();

  /*
  * Scrollmagic animation
  */
  App.scrollMagicController = new ScrollMagic.Controller();

  if ($('.chart').length) {
    //Donut Scene
    var stroke = $('.cht').attr('stroke-dasharray');

    if (stroke) {
      var donutScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.charts'
      })
      .setTween(TweenMax.from('.cht', 1.5, {
        strokeDashoffset:stroke
      }))
      .addTo(App.scrollMagicController);
    }
  }

  if ($('.reviews').length) {
  //Reviews Scene
    var ctr = { range: 50 };
    var $range = $('.reviews input[type="range"]');
    var reviewTween = new TimelineMax()
    .from(ctr, 1, {
      range: 0,
      onUpdate: function () {
        $range.val(Math.ceil(ctr.range)).change();
      }
    });

    var reviewScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.reviews'
    })
    .setTween(reviewTween)
    .addTo(App.scrollMagicController);
  }

  if ($('.featurette').length) {
    //Featurette Images
    new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.featurette-1',
      triggerHook: 0.7
    })
    .setTween(TweenMax.from('.featurette-1 .image', 0.5, {opacity:0}))
    .addTo(App.scrollMagicController);

    new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.featurette-2',
      triggerHook: 0.7
    })
    .setTween(TweenMax.from('.featurette-2 .image', 0.5, {opacity:0}))
    .addTo(App.scrollMagicController);

    new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.featurette-3',
      triggerHook: 0.7
    })
    .setTween(TweenMax.from('.featurette-3 .image', 0.5, {opacity:0}))
    .addTo(App.scrollMagicController);

    new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.featurette-4',
      triggerHook: 0.7
    })
    .setTween(TweenMax.from('.featurette-4 .image', 0.5, {opacity:0}))
    .addTo(App.scrollMagicController);
  }

  // if ($('.mod-header').length) {
  //   new ScrollMagic.Scene({
  //     triggerElement: 'body',
  //     triggerHook: 'onLeave',
  //     duration: '10%'
  //   })
  //   .setTween(TweenMax.to('.mod-header h1', 0.5, {
  //     opacity: 0,
  //     top: '-20px'
  //   }))
  //   .addTo(App.scrollMagicController);
  // }
  // if ($('.head-title').length) {
  //   new ScrollMagic.Scene({
  //     triggerElement: 'body',
  //     triggerHook: 'onLeave',
  //     duration: '10%'
  //   })
  //   .setTween(TweenMax.to('.head-title h1', 0.5, {
  //     opacity: 0,
  //     top: '-20px'
  //   }))
  //   .addTo(App.scrollMagicController);
  // }

  if ($('.review-bar-chart').length) {
  //Bar chart
    var barTween = new TimelineMax()
    .from('.beauty', 0.5, {height:'0%'})
    .from('.men', 0.5, {height:'0%'})
    .from('.women', 0.5, {height:'0%'})
    .from('.shoes', 0.5, {height:'0%'})
    .from('.bed', 0.5, {height:'0%'});

    var reviewBar = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.review-bar-chart'
    })
    .setTween(barTween)
    .addTo(App.scrollMagicController);
  }
  if ($('.section-5').length) {
  //How it works
    var howTween = new TimelineMax()
    .from('.step-1', 0.5, {opacity:0})
    .from('.step-2', 0.5, {opacity:0})
    .from('.step-3', 0.5, {opacity:0});

    var howScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.section-5'
    })
    .setTween(howTween)
    .addTo(App.scrollMagicController);
  }

  // check query string for modal auto-show
  (function(){
    var trk = getParameter("trk");
    if (trk == "request-demo") {
      // delay the show slightly for smoother animation (after page loads)
      setTimeout(function() {
        $("#request-demo-modal").modal('show');
      },800);
    }
  })();
});
