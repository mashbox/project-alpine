var gulp   = require('gulp');
var uglify = require('gulp-uglify');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('uglify', function() {
  return gulp.src(config.env.tmp + config.js.path + config.js.files)
    .on('error', handleErrors)
    // .pipe(uglify())
    .pipe(gulp.dest(config.env.tmp + config.js.path));
});