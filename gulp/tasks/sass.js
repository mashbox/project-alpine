var gulp    = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var config  = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('sass', function () {
  return gulp.src(config.env.src + config.sass.path + config.sass.files)
    .on('error', handleErrors)
    .pipe(sass({errLogToConsole: true}))
    .pipe(autoprefixer())
    // .pipe(cssnano())
    .pipe(gulp.dest(config.env.tmp + config.css.path));
});
