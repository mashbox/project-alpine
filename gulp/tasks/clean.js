var gulp   = require('gulp');
var del  = require('del');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

// Clean the temporary folder
gulp.task('clean-tmp', function () {
  return del([
    config.env.tmp + '/**/*.*'
  ]);
});

// Clean the build folder
gulp.task('clean-build', function () {
  return del([
    config.env.dst + '/**/*.*'
  ]);
});

// Clean the CSS Folder
gulp.task('clean-css', function () {
  return del([
    config.env.tmp + config.css.path + '/**/_*.css'
  ]);
});