var gulp   = require('gulp');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('fonts', function() {
  return gulp.src(config.env.src + config.fonts.path + config.fonts.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp + config.fonts.path));
});
