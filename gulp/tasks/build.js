var gulp   = require('gulp');
var runSequence = require('run-sequence');
var config = require('../config');

gulp.task('build', function (callback) {
  runSequence(['clean-tmp', 'clean-build'], ['images', 'fonts', 'css', 'markup', 'static', 'vendor'], 'sass', 'test', 'uglify', 'rev', 'clean-tmp', callback);
});
