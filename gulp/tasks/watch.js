/* Notes:
   - gulp/tasks/browserify.js handles js recompiling with watchify
   - gulp/tasks/browserSync.js watches and reloads compiled files
*/

var gulp    = require('gulp');
var browserSync = require('browser-sync');
var config  = require('../config');

gulp.task('watch', function() {
  gulp.watch(config.env.src + config.sass.path + config.sass.files, ['sass', browserSync.reload]);
  gulp.watch(config.env.src + config.images.path + config.images.files, ['images', browserSync.reload]);
  gulp.watch(config.env.src + config.fonts.path + config.fonts.files, ['fonts', browserSync.reload]);
  gulp.watch(config.env.src + config.markup.path + config.markup.files, ['markup', browserSync.reload]);
});
