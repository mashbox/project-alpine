var gulp        = require('gulp');
var rename      = require('gulp-rename');
var parallelize = require("concurrent-transform");
var awspublish  = require('gulp-awspublish');
var config      = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('deploy', ['build'], function () {
  var aws = require('../credentials.js').aws;
  var publisher = awspublish.create(aws);
  var headers = {'Cache-Control': 'max-age=86400, no-transform, public'};

 return gulp.src(config.env.dst + '/**')
   .on('error', handleErrors)
   .pipe(rename(function (path) {
      path.dirname = aws.path + '/' + path.dirname;
   }))
   .pipe(awspublish.gzip())
   .pipe(parallelize(publisher.publish(headers), 5))
   .pipe(publisher.cache())
   .pipe(awspublish.reporter());
});
