var gulp = require('gulp');
var config = require('../config')
var handleErrors = require('../util/handleErrors');

gulp.task('markup', function() {
  return gulp.src(config.env.src + config.markup.path + config.markup.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp));
});

gulp.task('static', function() {
  return gulp.src(config.env.src + config.static.path + config.static.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp + config.static.path));
});

gulp.task('vendor', function() {
  return gulp.src(config.env.src + '/javascript/vendor/**/*')
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp + '/js/vendor'));
});
